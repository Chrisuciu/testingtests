package com.qa.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.qa.code.Node;
import com.qa.code.Stack;

public class StackTest {

	Stack stack;
	Stack emptyStack;
	Node node;
	int valueForNode;
	@Before
	public void init(){
		valueForNode = 5;
		node = new Node(5);
		stack = new Stack(node);
		emptyStack = new Stack();
	}
	
	@Test
	public void testIsEmpty() {
		assertTrue(emptyStack.isEmpty() == true);
	}

	@Test
	public void testTop() {
		assertEquals(stack.top(), valueForNode);
	}

	@Test
	public void testPush() {
		stack.push(6);
		assertEquals(stack.top.getValue(), 6);
	}

	@Test
	public void testPop() {
		stack.push(6);
		stack.pop();
		assertEquals(stack.root.getValue(), 5);
	}

	@Test
	public void testElementAt() {
		stack.push(6);
		assertEquals(stack.elementAt(1), 6);
	}

	@Test
	public void testClone() {
		assertNotNull(stack.clone());
	}

}
